# cross-domain

code for paper `Cross-Domain Image Captioning via Cross-Modal Retrieval and Model Adaptation`

requirements:
- Python 3.8
- torch 1.7.0
- fairseq 0.10.1
- tqdm 

The working directory is `src`
